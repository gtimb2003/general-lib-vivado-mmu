This chapter describes how the MMU IP was implemented. It contains information about the structure of the IP and the implemented code. It also describes how the AXI4 Lite interfaces are implemented and how the finished module was incorporated in a final design. Explanations of the two FSM:s that provides the functionality of the MMU IP are included. A description of a test transaction is provided.




\section{RAM}
At the heart of the MMU is a rather simple design. As a lookup table, there is a 16 bit Random Access Memory design and all the routing and manipulating of the address is done with simple logic.

\begin{figure}[H]
\centering
\includegraphics[scale=0.8]{3}\\
\caption{Abstract MMU Design}
\label{fig:mmu}
\end{figure}
The RAM in this design is 16 bit(65536 cell) and is used to store the physical part of the translated addresses. To access the cell containing the physical part, the virtual part must be entered as the ram address.\\
\newpage

\begin{lstlisting}[caption=RAM VHDL Code]
5  entity ram is
6   port (
7      clock   : in  std_logic;
8      we      : in  std_logic;
9      address : in  std_logic_vector(15 downto 0);
10     datain  : in  std_logic_vector(15 downto 0);
11     dataout : out std_logic_vector(15 downto 0)
12    );
13 end entity ram;
14
15 architecture RTL of ram is
16
17   type ram_type is array (0 to (2**address'length)-1) of std_logic_vector(datain'range);
18   signal ram : ram_type;
19   signal read_address : std_logic_vector(address'range);
20
21 begin
22  RamProc: process(clock) is
23
24   begin
25    if rising_edge(clock) then
26      if we = '1' then
27        ram(to_integer(unsigned(address))) <= datain;
28      end if;
29      read_address <= address;
30    end if;
31  end process RamProc;
32
33  dataout <= ram(to_integer(unsigned(read_address)));
34
35 end architecture RTL;
\end{lstlisting}

\newpage

In order to save data at a specified address you have to enable the we signal and then enter the address you want to store and the data for storing.\\

\begin{figure}[H]
\centering
\includegraphics[scale=0.8]{4}\\
\caption{Ram Store Procedure}
\label{fig:mmu}
\end{figure}

In order to read from a address you only have to specify the address signal and the dataout signal will become equal to the stored data.

\begin{figure}[H]
\centering
\includegraphics[scale=0.8]{5}\\
\caption{Ram Read Procedure}
\label{fig:mmu}
\end{figure}
The code for the RAM is written in a versatile way that makes changes easy.\\
For example we can change the width by changing the address length on the line 9. 

\section{MMU Top Level}
In order to manage the routing and handling of the address a top level is needed.\\
In the top level, user logic must be added to "break" the virt(31 downto 0) signal into two (15 downto 0) signals. The first 16 bits are ported to the phys(15 downto 0) signal, at line 36 and the last 16 bits are ported to the address(15 downto 0) port of the ram component.\\

\begin{lstlisting}[caption=RAMs Port Map in Top Level]
29 begin
30 ram1: ram port map (
31        clock   => clock,
32        we      => we,
33        address => virt(31 downto 16),
34        datain  => datain,
35        dataout => phys(31 downto 16)
36        );
37        phys(15 downto 0) <= virt(15 downto 0);
38        
\end{lstlisting}

This enables to pair the translated part, sourced from the RAM, with the non-translated part of the Virtual address.\\


\section{AXI Interface}

In order to include the MMU in a working design we must "encapsulate" is with an AXI4 interface in order to be able to connect it with other IP blocks that implement the same protocol.\\
The MMU IP will have 2 AXI4 interfaces. One AXI4 Lite Slave port will be utilised as the IPs input from the PS and one AXI4 Lite Master port used from the IP to write on a target(BRAM, DDR e.t.c.)\\

\begin{figure}[H]
\centering
\includegraphics[scale=0.9]{17}\\
\caption{AXI4 Interface Design}
\label{fig:abstractip}
\end{figure}

\subsection{Slave AXI Interface}
The Slave interface will have 4 Input Registers where input data will be entered. Each register will serve a specific purpose.

\begin{table}[H]
\begin{tabular}{c | c | c | c | p{5cm}}
Register & Address & Use & Size\\
\hline \hline
0 & 0x43C00000 & Targeted/Translated Address & 16 bit/32 bit\\
\hline
1 & 0x43C00004 & Write Enable/Association Address & 1 bit/16 bit\\ 
\hline
2 & 0x43C00008 & AXI4 Lite Transaction Initialisation & 1 bit\\
\hline
3 & 0x43C0000C & Data & 32 bit

\end{tabular}
\caption{MMU Slave Registers}
\end{table}

\begin{description}
  \item[·Register 0] \hfill \\
  This is a 32 bit register that will have dual purpose. In translation mode it will act as the Virtual Address input. In store mode its first 16 bits will be used as the RAM target address.
  \item[·Register 1] \hfill \\
  This 32 bit register utilises only 17 bits. Bit 16 is used to control the mode of the MMU. If it is 'true' we are in Store mode. If it is 'false' we are in Translation Mode. The last 16 bits are used only when we are in Store Mode, ie when the 16th bit of Register 1 is '1'. The data that will be stored in the RAM address specified by Register 0, will be input through this Register. 
  \item[·Register 2] \hfill \\
  Register 2 is a control register that initialises the AXI transaction when enabled. When we enable it (i.e. 0x43C00000 = '1') the MMU writes the data we input on the specified target using the translated address.
  \item[·Register 3] \hfill \\
  This is the data input Register. It is a 32 bit Register used as an input for the data we want to store.
\end{description}

Below you can see the port mapping of the MMU module, inside the Slave port and how we assign each Register to the MMUs input.\\

\begin{lstlisting}[caption=Port Map of MMU in Slave Port]
263	mmu_0 : mmu
264	port map(
265        virt  => slv_reg0,
266        phys  => s_out_port0,
267        clock => S_AXI_ACLK,
268        we    => slv_reg1(16),
269        datain=> slv_reg1(15 downto 0)
270         );
271
272    s_txn_init <=slv_reg2(0);
273    s_out_port1 <=slv_reg3;
\end{lstlisting}


The Slave interface will also have 2 OUT ports(s\_out\_port0 and s\_out\_port1) used for communication between the Slave and Master port, within the IP. These ports will be used to transfer the translated address(line 266) and data(line 273) to the Master port in order for the Master port to perform the AXI write. To initialize the AXI Write on the Master there is a s\_txn\_init out port that is controlled, as marked before, from Register 2(line 272).\\

\subsection{Master AXI Interface}
The master interface will be implemented to enable the IP to complete AXI Writes to other AXI Slave ports in order to store data to specified targets.\\
The address, which at this point is already translated by the MMU in the Slave port, and data are input from the m\_in\_port0 and m\_in\_port1(line 14 and 15) to the Master interface and ported through 2 signals(line 440 and 441) to the M\_AXI\_AWADDR and M\_AXI\_WDATA OUT ports(line 114 and 115).\\
This essentially tells the Master port where to store the data that it got.\\

\begin{lstlisting}[caption=Master Port Routing Routine]
		14  m_in_port0 : in std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
		15  m_in_port1 : in std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
		.
		.
		114 M_AXI_AWADDR <= mmu_transfer0;
		115 M_AXI_WDATA	 <= mmu_transfer1;
		.
		.
		440 mmu_transfer0 <= m_in_port0;
		441 mmu_transfer1 <= m_in_port1;
\end{lstlisting}
\newpage

\subsection{Complete IP}
After we finish writing and editing the code for the AXI4 Interfaces, with the use of Vivados Create and Package IP tool, we package everything into one IP that can be used in the Block Design environment as a IP Block.\\

\begin{figure}[H]
\centering
\includegraphics[scale=0.6]{21}\\
\caption{Packaged MMU IP Block}
\label{fig:zynq}
\end{figure}

The tool automatically produces an IP with the appropriate ports that can be later added to a block diagram in Vivados IP Integrator.\\
The IP, besides the AXI Ports, has various other ports. The most important ports are the Clock ports(axi\_aclk), that provide clock timing to the ports and the reset ports(sxi\_aresetn) that provide reset signaling for the AXI Ports. All the other ports are rendered useless for our purpose.\\
\newpage
\subsection{IP Modes}
By completing this IP we get an MMU IP with 2 AXI4 Ports. This MMU has 2 modes. The Translation Mode where it translation of an address will occur, according to the translation table that is configured in the Store mode.\\
The modes are user swappable and can be controlled through Register 1.\\
\subsubsection{Store Mode}
Store mode enables us to append a Physical Part to a Virtual Part of an address in order for it to be translated at a later time.\\
The whole process starts by giving an almost arbitrary 16 bit address, that will serve as the Virtual Part, to Register 0. Then another 16 bit address must be given at Register 1. This will be the 16 bits that the Virtual Address will be translated to. At the same time we have to enable Bit 16 of Register 1 so that the MMU will be put in Store Mode and store the translation in its RAM.\\

\begin{figure}[H]
\centering
\includegraphics[scale=0.7]{24}\\
\caption{Store Mode FSM}
\label{fig:smfsm}
\end{figure}

In the event that we need to perform another Store, Bit 16 of Register 1 must be set to '0' and the process must start from the beginning. If this routine is not followed, overwrite issues may occur.\\

\subsubsection{Translation Mode}
Translation Mode is the "Normal" mode of the MMU in which it accepts a Virtual Address and Data, storing the Data on the Translated Address.\\
It all starts by giving the Virtual Address on Register 0. After that, the Data for storing must be given on Register 3. Then simply enable the AXI Transaction signal on Register 2 and the MMU will translate the address and store the Data on the translated address.\\

\begin{figure}[H]
\centering
\includegraphics[scale=0.7]{25}\\
\caption{Translation Mode FSM}
\label{fig:tmfsm}
\end{figure}

In order to perform another translation, the AXI Transaction signal on Register 2 must be set to "0" again, to avoid overwriting existing data.\\

\newpage


\section{Design}
The main focus of the Design is enable us to test multiple scenarios. For example we want to have multiple targets, where we could write, so that we can test the MMUs ability to translate and write on multiple targets as well as in different bases within the same target.\\
In order to implement the design we use Vivados IP intergrator and we create a new Block Design. Within this Block design we will add our IP Blocks and connect them appropriately
\subsection{PS}

The Processing System 7 core is the software interface around the Zynq-7000 platform processing system. The Zynq-7000 family consists of an SoC style integrated PS and a PL unit, providing an extensible and flexible SoC solution on a single die. The Processing System 7 core acts as a logic connection between the PS and the PL while assisting you to integrate customized and embedded IP cores with the processing system using the Vivado® IP integrator.\\

\begin{figure}[H]
\centering
\includegraphics[scale=0.6]{18}\\
\caption{Zynq Processing System}
\label{fig:zynq}
\end{figure}

The Processing System 7 wrapper instantiates the processing system section of the Zynq®-7000 All Programmable SoC for the programmable logic and external board logic. The wrapper includes unaltered connectivity and, for some signals, some logic functions. For a description of the architecture of the processing system, see the Zynq-7000 All Programmable SoC Technical Reference Manual (UG585)\\

\newpage
\subsection{Targets}
As mentioned before, we need to have multiple targets in order to test various scenarios. For this Design we chose to use 2 Memory Blocks(one 128k and one 8k) and the DDR Memory of the Zedboard.\\
The Memory Blocks will be Dual Port ones to make us able to access them both from the PS and the MMU.

\begin{figure}[H]
\centering
\includegraphics[scale=0.6]{19}\\
\caption{Targets}
\label{fig:targets}
\end{figure}

The MMU IP will access the Block Memory through its AXI4 Master Port. It will also access the DDR but this time through the Zynq PS HP0 AXI4 Port.\\
The Zynq® PS will also access the Block Memory through its AXI4 GP0 Port in order to confirm the correct function of the MMU IP.\\
In our design we will use an AXI BRAM Controller to enable us to access the Block Memory through an AXI4 Port. The DDR does not need such a controlled due to the fact that the PS has an integrated DDR Controller\\

\begin{figure}[H]
\centering
\includegraphics[scale=0.6]{22}\\
\caption{AXI BRAM Controller}
\label{fig:bramctrl}
\end{figure}
\newpage
\subsection{AXI Interconnects}

The AXI Interconnect core can be added to a Vivado® IP integrator block design in the Vivado Design Suite. The Interconnect IP core represents a hierarchical design block that become configured and connected during our system design session.\\
It allows any mixture of AXI master and slave devices to be connected to it, which can vary from one another in terms of data width, clock domain and AXI sub-protocol (AXI4, AXI3, or AXI4 Lite). When the interface characteristics of any connected master or slave device differ from those of the crossbar switch inside the interconnect, the appropriate infrastructure cores are automatically inferred and connected within the interconnect to perform the necessary conversions.\\

\begin{figure}[H]
\centering
\includegraphics[scale=0.4]{20}\\
\caption{Interconnect}
\label{fig:Interconnect}
\end{figure}

In this design we use the AXI Interconnects in order for the MMU and the PS to access all three Targets through one Master port.\\ 

\newpage 

\subsection{Complete Design}

Merging it all together we end up with the following design.\\

\begin{figure}[H]
\centering
\includegraphics[scale=0.7]{23}\\
\caption{Complete Design}
\label{fig:design}
\end{figure}

Vivado automatically configured the connection with the DDR.\\
After everything is set we let Vivado generate the Bitstream that will later be downloaded on the Zedboard so that we can test the MMU.\\
