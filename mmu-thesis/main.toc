\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Memmory Managment Units}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Overview of FPGAs}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Purpose}{3}{section.1.3}
\contentsline {chapter}{\numberline {2}Tools}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Hardware}{4}{section.2.1}
\contentsline {subsubsection}{ZedBoard\IeC {\texttrademark }}{4}{section*.9}
\contentsline {section}{\numberline {2.2}Software}{5}{section.2.2}
\contentsline {subsubsection}{Vivado\IeC {\textregistered }}{5}{section*.11}
\contentsline {subsubsection}{SDK}{5}{section*.13}
\contentsline {subsubsection}{AMBA AXI Interface}{5}{section*.14}
\contentsline {section}{\numberline {2.3}Used Languages}{6}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}VHDL}{6}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}TCL}{6}{subsection.2.3.2}
\contentsline {chapter}{\numberline {3}Implementation}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}RAM}{7}{section.3.1}
\contentsline {section}{\numberline {3.2}MMU Top Level}{9}{section.3.2}
\contentsline {section}{\numberline {3.3}AXI Interface}{10}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Slave AXI Interface}{11}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Master AXI Interface}{12}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Complete IP}{13}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}IP Modes}{14}{subsection.3.3.4}
\contentsline {subsubsection}{Store Mode}{14}{section*.21}
\contentsline {subsubsection}{Translation Mode}{14}{section*.23}
\contentsline {section}{\numberline {3.4}Design}{16}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}PS}{16}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Targets}{17}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}AXI Interconnects}{18}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Complete Design}{19}{subsection.3.4.4}
\contentsline {chapter}{\numberline {4}Testing}{20}{chapter.4}
\contentsline {section}{\numberline {4.1}Ram Testing}{20}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}TestBench}{20}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Results}{21}{subsection.4.1.2}
\contentsline {section}{\numberline {4.2}Top Level Testing}{23}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}TestBench}{23}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Results}{24}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}IP Testing}{25}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Testing Methodology}{27}{subsection.4.3.1}
\contentsline {subsubsection}{Testing Scripts}{27}{section*.37}
\contentsline {chapter}{\numberline {5}Conclusions}{30}{chapter.5}
\contentsline {section}{\numberline {5.1}Future Work}{30}{section.5.1}
\contentsline {chapter}{\numberline {A}Abbreviations}{31}{appendix.A}
\contentsline {chapter}{\numberline {B}.tcl Scripts}{33}{appendix.B}
\contentsline {section}{\numberline {B.1}test.tcl}{33}{section.B.1}
\contentsline {section}{\numberline {B.2}call.tcl}{34}{section.B.2}
\contentsline {section}{\numberline {B.3}store.tcl}{34}{section.B.3}
\contentsline {subsubsection}{Copyrights}{38}{section*.43}
