library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity myMMU_v1_0_M00_AXI is
	generic (
		C_M_START_DATA_VALUE	: std_logic_vector	:= x"AA000000";
		C_M_TARGET_SLAVE_BASE_ADDR	: std_logic_vector	:= x"40000000";
		C_M_AXI_ADDR_WIDTH	: integer	:= 32;
		C_M_AXI_DATA_WIDTH	: integer	:= 32;
		C_M_TRANSACTIONS_NUM	: integer	:= 4
	);
	port (
		m_in_port0 : in std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
		m_in_port1 : in std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);

		INIT_AXI_TXN	: in std_logic;
		ERROR	: out std_logic;
		TXN_DONE	: out std_logic;
		M_AXI_ACLK	: in std_logic;
		M_AXI_ARESETN	: in std_logic;
		M_AXI_AWADDR	: out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
		M_AXI_AWPROT	: out std_logic_vector(2 downto 0);
		M_AXI_AWVALID	: out std_logic;
		M_AXI_AWREADY	: in std_logic;
		M_AXI_WDATA	: out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
		M_AXI_WSTRB	: out std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0);
		M_AXI_WVALID	: out std_logic;
		M_AXI_WREADY	: in std_logic;
		M_AXI_BRESP	: in std_logic_vector(1 downto 0);
		M_AXI_BVALID	: in std_logic;
		M_AXI_BREADY	: out std_logic;
		M_AXI_ARADDR	: out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
		M_AXI_ARPROT	: out std_logic_vector(2 downto 0);
		M_AXI_ARVALID	: out std_logic;
		M_AXI_ARREADY	: in std_logic;
		M_AXI_RDATA	: in std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
		M_AXI_RRESP	: in std_logic_vector(1 downto 0);
		M_AXI_RVALID	: in std_logic;
		M_AXI_RREADY	: out std_logic
	);
end myMMU_v1_0_M00_AXI;

architecture implementation of myMMU_v1_0_M00_AXI is
        signal mmu_transfer0	: std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);--signal for mmu addr out
        signal mmu_transfer1	: std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);--signal for mmu data out

	function clogb2 (bit_depth : integer) return integer is            
	 	variable depth  : integer := bit_depth;                               
	 	variable count  : integer := 1;                                       
	 begin                                                                   
	 	 for clogb2 in 1 to bit_depth loop  -- Works for up to 32 bit integers
	      if (bit_depth <= 2) then                                           
	        count := 1;                                                      
	      else                                                               
	        if(depth <= 1) then                                              
	 	       count := count;                                                
	 	     else                                                             
	 	       depth := depth / 2;                                            
	          count := count + 1;                                            
	 	     end if;                                                          
	 	   end if;                                                            
	   end loop;                                                             
	   return(count);        	                                              
	 end;                                                                    

	 constant  TRANS_NUM_BITS  : integer := clogb2(C_M_TRANSACTIONS_NUM-1);


	 type state is ( IDLE, -- This state initiates AXI4Lite transaction
	 							-- after the state machine changes state to INIT_WRITE
	 							-- when there is 0 to 1 transition on INIT_AXI_TXN
	 				INIT_WRITE,   -- This state initializes write transaction,
	 							-- once writes are done, the state machine 
	 							-- changes state to INIT_READ 
	 				INIT_READ,    -- This state initializes read transaction
	 							-- once reads are done, the state machine 
	 							-- changes state to INIT_COMPARE 
	 				INIT_COMPARE);-- This state issues the status of comparison 
	 							-- of the written data with the read data

	 signal mst_exec_state  : state ; 

	signal axi_awvalid	: std_logic;
	signal axi_wvalid	: std_logic;
	signal axi_arvalid	: std_logic;
	signal axi_rready	: std_logic;
	signal axi_bready	: std_logic;
	signal axi_awaddr	: std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
	signal axi_wdata	: std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
	signal axi_araddr	: std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
	signal write_resp_error	: std_logic;
	signal read_resp_error	: std_logic;
	signal start_single_write	: std_logic;
	signal start_single_read	: std_logic;
	signal write_issued	: std_logic;
	signal read_issued	: std_logic;
	signal writes_done	: std_logic;
	signal reads_done	: std_logic;
	signal error_reg	: std_logic;
	signal write_index	: std_logic_vector(TRANS_NUM_BITS downto 0);
	signal read_index	: std_logic_vector(TRANS_NUM_BITS downto 0);
	signal expected_rdata	: std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
	signal compare_done	: std_logic;
	signal read_mismatch	: std_logic;
	signal last_write	: std_logic;
	signal last_read	: std_logic;
	signal init_txn_ff	: std_logic;
	signal init_txn_ff2	: std_logic;
	signal init_txn_edge	: std_logic;
	signal init_txn_pulse	: std_logic;

begin
	M_AXI_AWADDR	<= mmu_transfer0; --std_logic_vector (unsigned(C_M_TARGET_SLAVE_BASE_ADDR) + unsigned(axi_awaddr));
	M_AXI_WDATA	<= mmu_transfer1; --axi_wdata;
	M_AXI_AWPROT	<= "000";
	M_AXI_AWVALID	<= axi_awvalid;
	M_AXI_WVALID	<= axi_wvalid;
	M_AXI_WSTRB	<= "1111";
	M_AXI_BREADY	<= axi_bready;
	M_AXI_ARADDR	<= std_logic_vector(unsigned(C_M_TARGET_SLAVE_BASE_ADDR) + unsigned(axi_araddr));
	M_AXI_ARVALID	<= axi_arvalid;
	M_AXI_ARPROT	<= "001";
	M_AXI_RREADY	<= axi_rready;
	TXN_DONE	<= compare_done;
	init_txn_pulse	<= ( not init_txn_ff2)  and  init_txn_ff;

	process(M_AXI_ACLK)                                                          
	begin                                                                             
	  if (rising_edge (M_AXI_ACLK)) then                                              
	    if (M_AXI_ARESETN = '0' ) then                                                
	      init_txn_ff <= '0';                                                   
	        init_txn_ff2 <= '0';                                                          
	    else                                                                                       
	      init_txn_ff <= INIT_AXI_TXN;
	        init_txn_ff2 <= init_txn_ff;                                                                     
	    end if;                                                                       
	  end if;                                                                         
	end process; 

	  process(M_AXI_ACLK)                                                          
	  begin                                                                             
	    if (rising_edge (M_AXI_ACLK)) then                                              
	      if (M_AXI_ARESETN = '0' or init_txn_pulse = '1') then                                                
	        axi_awvalid <= '0';                                                         
	      else                                                                          
	        if (start_single_write = '1') then                                          
	          axi_awvalid <= '1';                                                       
	        elsif (M_AXI_AWREADY = '1' and axi_awvalid = '1') then                      
	          axi_awvalid <= '0';                                                       
	        end if;                                                                     
	      end if;                                                                       
	    end if;                                                                         
	  end process;                                                                      
	                                                                                    
                                                              
	  process(M_AXI_ACLK)                                                               
	  begin                                                                             
	    if (rising_edge (M_AXI_ACLK)) then                                              
	      if (M_AXI_ARESETN = '0' or init_txn_pulse = '1') then                                                
	        write_index <= (others => '0');                                             
	      elsif (start_single_write = '1') then                                         
	        -- Signals a new write address/ write data is                               
	        -- available by user logic                                                  
	        write_index <= std_logic_vector (unsigned(write_index) + 1);                                           
	      end if;                                                                       
	    end if;                                                                         
	  end process;                                                                      

	   process(M_AXI_ACLK)                                                 
	   begin                                                                         
	     if (rising_edge (M_AXI_ACLK)) then                                          
	       if (M_AXI_ARESETN = '0' or init_txn_pulse = '1' ) then                                            
	         axi_wvalid <= '0';                                                      
	       else                                                                      
	         if (start_single_write = '1') then                                      
	           axi_wvalid <= '1';                                                    
	         elsif (M_AXI_WREADY = '1' and axi_wvalid = '1') then                    
	           axi_wvalid <= '0';                                                    
	         end if;                                                                 
	       end if;                                                                   
	     end if;                                                                     
	   end process;                                                                  

	  process(M_AXI_ACLK)                                            
	  begin                                                                
	    if (rising_edge (M_AXI_ACLK)) then                                 
	      if (M_AXI_ARESETN = '0' or init_txn_pulse = '1') then                                   
	        axi_bready <= '0';                                             
	      else                                                             
	        if (M_AXI_BVALID = '1' and axi_bready = '0') then              
	           axi_bready <= '1';                                          
	        elsif (axi_bready = '1') then                                  
	          axi_bready <= '0';                                           
	        end if;                                                        
	      end if;                                                          
	    end if;                                                            
	  end process;                                                         
	  write_resp_error <= (axi_bready and M_AXI_BVALID and M_AXI_BRESP(1));

	  process(M_AXI_ACLK)                                                              
	  begin                                                                            
	    if (rising_edge (M_AXI_ACLK)) then                                             
	      if (M_AXI_ARESETN = '0' or init_txn_pulse = '1') then                                               
	        read_index <= (others => '0');                                             
	      else                                                                         
	        if (start_single_read = '1') then                                          
	          read_index <= std_logic_vector (unsigned(read_index) + 1);                                          
	        end if;                                                                    
	      end if;                                                                      
	    end if;                                                                        
	  end process;                                                                     
	                                                                                                                                                
	  process(M_AXI_ACLK)                                                              
	  begin                                                                            
	    if (rising_edge (M_AXI_ACLK)) then                                             
	      if (M_AXI_ARESETN = '0' or init_txn_pulse = '1') then                                               
	        axi_arvalid <= '0';                                                        
	      else                                                                         
	        if (start_single_read = '1') then                                          
	          axi_arvalid <= '1';                                                      
	        elsif (M_AXI_ARREADY = '1' and axi_arvalid = '1') then                     
	          axi_arvalid <= '0';                                                      
	        end if;                                                                    
	      end if;                                                                      
	    end if;                                                                        
	  end process;                                                                     

	  process(M_AXI_ACLK)                                             
	  begin                                                                 
	    if (rising_edge (M_AXI_ACLK)) then                                  
	      if (M_AXI_ARESETN = '0' or init_txn_pulse = '1') then                                    
	        axi_rready <= '1';                                              
	      else                                                              
	        if (M_AXI_RVALID = '1' and axi_rready = '0') then               
	          axi_rready <= '1';                                            
	        elsif (axi_rready = '1') then                                   
	          axi_rready <= '0';                                            
	        end if;                                                         
	      end if;                                                           
	    end if;                                                             
	  end process;                                                          
	                                                                        
	  read_resp_error <= (axi_rready and M_AXI_RVALID and M_AXI_RRESP(1));  

                                                  
	    process(M_AXI_ACLK)                                                                 
	      begin                                                                            
	    	if (rising_edge (M_AXI_ACLK)) then                                              
	    	  if (M_AXI_ARESETN = '0' or init_txn_pulse = '1') then                                                
	    	    axi_awaddr <= (others => '0');                                              
	    	  elsif (M_AXI_AWREADY = '1' and axi_awvalid = '1') then                        
	    	    axi_awaddr <= std_logic_vector (unsigned(axi_awaddr) + 4);                                     
	    	  end if;                                                                       
	    	end if;                                                                         
	      end process;                                                                     
	                                                                                       
	    process(M_AXI_ACLK)                                                                
	   	  begin                                                                         
	   	    if (rising_edge (M_AXI_ACLK)) then                                          
	   	      if (M_AXI_ARESETN = '0' or init_txn_pulse = '1' ) then                                            
	   	        axi_araddr <= (others => '0');                                          
	   	      elsif (M_AXI_ARREADY = '1' and axi_arvalid = '1') then                    
	   	        axi_araddr <= std_logic_vector (unsigned(axi_araddr) + 4);                                 
	   	      end if;                                                                   
	   	    end if;                                                                     
	   	  end process;                                                                  
		                                                                                    
	    process(M_AXI_ACLK)                                                                
		  begin                                                                             
		    if (rising_edge (M_AXI_ACLK)) then                                              
		      if (M_AXI_ARESETN = '0' or init_txn_pulse = '1') then                                                
		        axi_wdata <= C_M_START_DATA_VALUE;    	                                    
		      elsif (M_AXI_WREADY = '1' and axi_wvalid = '1') then                          
		        axi_wdata <= std_logic_vector (unsigned(C_M_START_DATA_VALUE) + unsigned(write_index));    
		      end if;                                                                       
		    end if;                                                                         
		  end process;                                                                      
		                                                                                    		                                                                                    
	    process(M_AXI_ACLK)                                                                
	    begin                                                                              
	      if (rising_edge (M_AXI_ACLK)) then                                               
	        if (M_AXI_ARESETN = '0' or init_txn_pulse = '1' ) then                                                 
	          expected_rdata <= C_M_START_DATA_VALUE;    	                                
	        elsif (M_AXI_RVALID = '1' and axi_rready = '1') then                           
	          expected_rdata <= std_logic_vector (unsigned(C_M_START_DATA_VALUE) + unsigned(read_index)); 
	        end if;                                                                        
	      end if;                                                                          
	    end process;                                                                       
	  MASTER_EXECUTION_PROC:process(M_AXI_ACLK)                                                         
	  begin                                                                                             
	    if (rising_edge (M_AXI_ACLK)) then                                                              
	      if (M_AXI_ARESETN = '0' ) then                                                                
	        mst_exec_state  <= IDLE;                                                            
	        start_single_write <= '0';                                                                  
	        write_issued   <= '0';                                                                      
	        start_single_read  <= '0';                                                                  
	        read_issued  <= '0';                                                                        
	        compare_done   <= '0';                                                                      
	        ERROR <= '0'; 
	      else                                                                                          
	        case (mst_exec_state) is                                                                    
	                                                                                                    
	          when IDLE =>                                                                      
	            if ( init_txn_pulse = '1') then    
	              mst_exec_state  <= INIT_WRITE;                                                        
	              ERROR <= '0';
	              compare_done <= '0';
	            else                                                                                    
	              mst_exec_state  <= IDLE;                                                      
	            end if;                                                                                 
	                                                                                                    
	          when INIT_WRITE =>                                                                        
	            if (writes_done = '1') then                                                             
	              mst_exec_state <= INIT_READ;                                                          
	            else                                                                                    
	              mst_exec_state  <= INIT_WRITE;                                                        
	                                                                                                    
	              if (axi_awvalid = '0' and axi_wvalid = '0' and M_AXI_BVALID = '0' and                 
	                last_write = '0' and start_single_write = '0' and write_issued = '0') then          
	                start_single_write <= '1';                                                          
	                write_issued  <= '1';                                                               
	              elsif (axi_bready = '1') then                                                         
	                write_issued   <= '0';                                                              
	              else                                                                                  
	                start_single_write <= '0'; --Negate to generate a pulse                             
	              end if;                                                                               
	            end if;                                                                                 
	                                                                                                    
	          when INIT_READ =>                                                                                                                                         
	            if (reads_done = '1') then                                                              
	              mst_exec_state <= INIT_COMPARE;                                                       
	            else                                                                                    
	              mst_exec_state  <= INIT_READ;                                                         
	                                                                                                    
	              if (axi_arvalid = '0' and M_AXI_RVALID = '0' and last_read = '0' and                  
	                start_single_read = '0' and read_issued = '0') then                                 
	                start_single_read <= '1';                                                           
	                read_issued   <= '1';                                                               
	              elsif (axi_rready = '1') then                                                         
	                read_issued   <= '0';                                                               
	              else                                                                                  
	                start_single_read <= '0'; --Negate to generate a pulse                              
	              end if;                                                                               
	            end if;                                                                                 
	                                                                                                    
	          when INIT_COMPARE =>                                                                                               
	            ERROR <= error_reg;                                                               
	            mst_exec_state <= IDLE;                                                       
	            compare_done <= '1';                                                                  
	                                                                                                    
	          when others  =>                                                                           
	              mst_exec_state  <= IDLE;                                                      
	        end case  ;                                                                                 
	      end if;                                                                                       
	    end if;                                                                                         
	  end process;                                                                                      
	                                                                                                    
	  process(M_AXI_ACLK)                                                                               
	  begin                                                                                             
	    if (rising_edge (M_AXI_ACLK)) then                                                              
	      if (M_AXI_ARESETN = '0' or init_txn_pulse = '1') then                                                                
	        last_write <= '0';                                                                          
	      else                                                                                          
	        if (write_index = STD_LOGIC_VECTOR(TO_UNSIGNED(C_M_TRANSACTIONS_NUM, TRANS_NUM_BITS+1)) and M_AXI_AWREADY = '1') then
	          last_write  <= '1';                                                                       
	        end if;                                                                                     
	      end if;                                                                                       
	    end if;                                                                                         
	  end process;                                                                                      
	                                                                                                                                                                                     
	  process(M_AXI_ACLK)                                                                               
	  begin                                                                                             
	    if (rising_edge (M_AXI_ACLK)) then                                                              
	      if (M_AXI_ARESETN = '0' or init_txn_pulse = '1') then                                                                
	        -- reset condition                                                                          
	        writes_done <= '0';                                                                         
	      else                                                                                          
	        if (last_write = '1' and M_AXI_BVALID = '1' and axi_bready = '1') then                      
	          --The writes_done should be associated with a bready response                             
	          writes_done <= '1';                                                                       
	        end if;                                                                                     
	      end if;                                                                                       
	    end if;                                                                                         
	  end process;                                                                                      
	                                                                                                    	                                                                                                    
	  process(M_AXI_ACLK)                                                                               
	  begin                                                                                             
	    if (rising_edge (M_AXI_ACLK)) then                                                              
	      if (M_AXI_ARESETN = '0' or init_txn_pulse = '1') then                                                                
	        last_read <= '0';                                                                           
	      else                                                                                          
	        if (read_index = STD_LOGIC_VECTOR(TO_UNSIGNED(C_M_TRANSACTIONS_NUM, TRANS_NUM_BITS+1)) and (M_AXI_ARREADY = '1') ) then
	          last_read <= '1';                                                                         
	        end if;                                                                                     
	      end if;                                                                                       
	    end if;                                                                                         
	  end process;                                                                                      
	                                                                                                                                                                                          
	  process(M_AXI_ACLK)                                                                               
	  begin                                                                                             
	    if (rising_edge (M_AXI_ACLK)) then                                                              
	      if (M_AXI_ARESETN = '0' or init_txn_pulse = '1') then                                                                
	        reads_done <= '0';                                                                          
	      else                                                                                          
	        if (last_read = '1' and M_AXI_RVALID = '1' and axi_rready = '1') then                       
	          reads_done <= '1';                                                                        
	        end if;                                                                                     
	      end if;                                                                                       
	    end if;                                                                                         
	  end process;                                                                                      
	                                                                                                    
	                                                                                                                                                                                  
	  process(M_AXI_ACLK)                                                                               
	  begin                                                                                             
	    if (rising_edge (M_AXI_ACLK)) then                                                              
	      if (M_AXI_ARESETN = '0' or init_txn_pulse = '1') then                                                                
	        read_mismatch <= '0';                                                                       
	      else                                                                                          
	        if ((M_AXI_RVALID = '1' and axi_rready = '1') and  M_AXI_RDATA /= expected_rdata) then      
	          read_mismatch <= '1';                                                                     
	        end if;                                                                                     
	      end if;                                                                                       
	    end if;                                                                                         
	  end process;                                                                                      
	                                                                                                    
	  process(M_AXI_ACLK)                                                                               
	  begin                                                                                             
	    if (rising_edge (M_AXI_ACLK)) then                                                              
	      if (M_AXI_ARESETN = '0' or init_txn_pulse = '1') then                                                                
	        error_reg <= '0';                                                                           
	      else                                                                                          
	        if (read_mismatch = '1' or write_resp_error = '1' or read_resp_error = '1') then            
	          error_reg <= '1';                                                                         
	        end if;                                                                                     
	      end if;                                                                                       
	    end if;                                                                                         
	  end process;                                                                                      

	mmu_transfer0 <= m_in_port0;
	
	mmu_transfer1 <= m_in_port1;
	
end implementation;
