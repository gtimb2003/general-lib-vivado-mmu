This project is my "Development and Implementation Of An AXI MMU".
It contains the documentation and implementation.
It was implementad on Vivado 2015.1
mmu-design contains the Vivado implementation.
mmu-presentation contains the Thesis powerpoint presentation.
mmu-spec-sheet contains the latex files of a brief manual for the mmu.
mmu-thesis contains the thesis latex files.
thesis-jour contains the thesis in a paper form.
