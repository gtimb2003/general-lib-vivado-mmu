\contentsline {chapter}{\numberline {1}Design Setup}{5}
\contentsline {section}{\numberline {1.1}File Acquisition}{5}
\contentsline {section}{\numberline {1.2}Files}{5}
\contentsline {subsection}{\numberline {1.2.1}Design Diagram}{6}
\contentsline {subsection}{\numberline {1.2.2}Address Editor}{7}
\contentsline {subsubsection}{MMU Addressing}{7}
\contentsline {subsubsection}{Memory Block Addressing}{7}
\contentsline {section}{\numberline {1.3}Running Design}{8}
\contentsline {subsection}{\numberline {1.3.1}Exporting}{8}
\contentsline {subsection}{\numberline {1.3.2}SDK}{8}
\contentsline {subsubsection}{Programming FPGA}{8}
\contentsline {subsubsection}{Initialising}{8}
\contentsline {chapter}{\numberline {2}Design Run}{10}
\contentsline {section}{\numberline {2.1}XMD}{10}
\contentsline {section}{\numberline {2.2}MMU Functional Specification}{11}
\contentsline {subsection}{\numberline {2.2.1}Slave Registers}{12}
\contentsline {subsubsection}{Register 1}{12}
\contentsline {subsubsection}{Register 2}{12}
\contentsline {subsubsection}{Register 3}{12}
\contentsline {subsubsection}{Register 4}{12}
\contentsline {subsection}{\numberline {2.2.2}Simple Example}{13}
\contentsline {subsubsection}{Store Mode}{13}
\contentsline {subsubsection}{Translation Mode}{13}
\contentsline {subsection}{\numberline {2.2.3}Multi-Target Example}{14}
\contentsline {subsubsection}{Store Mode}{14}
\contentsline {subsubsection}{Translation Mode}{15}
\contentsline {chapter}{\numberline {3}Conclusion}{18}
