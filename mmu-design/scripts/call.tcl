puts "Give Address"
set addr [gets stdin]

puts "Give Data"
set data [gets stdin]

# Set address reg
mwr [expr 0x43C00000] $addr
# Set data reg
mwr [expr 0x43C0000c] $data

mwr [expr 0x43C00008] [expr 0x11111111]
mwr [expr 0x43C00008] [expr 0x00000000]