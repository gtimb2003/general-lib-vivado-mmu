#store 1st Translation
mwr [expr 0x43C00000] [expr 0x11110000]
mwr [expr 0x43C00004] [expr 0x11110000]
mwr [expr 0x43C00004] [expr 0x00000000]
#store 2nd Translation
mwr [expr 0x43C00000] [expr 0x22220000]
mwr [expr 0x43C00004] [expr 0x111143C0]
mwr [expr 0x43C00004] [expr 0x00000000]
#store 3rd Translation
mwr [expr 0x43C00000] [expr 0x33330000]
mwr [expr 0x43C00004] [expr 0x1111C000]
mwr [expr 0x43C00004] [expr 0x00000000]
#store 4th Translation
mwr [expr 0x43C00000] [expr 0x44440000]
mwr [expr 0x43C00004] [expr 0x1111C001]
mwr [expr 0x43C00004] [expr 0x00000000]
#Store Random Data On 1st Target Using Virtual Base Address
mwr [expr 0x43C00000] [expr 0x1111000C]
mwr [expr 0x43C0000c] [expr 0xAAAAAAAA]
mwr [expr 0x43C00008] [expr 0x11111111]
mwr [expr 0x43C00008] [expr 0x00000000]
#Store Random Data On 2nd Target Using Virtual Base Address
mwr [expr 0x43C00000] [expr 0x2222001C]
mwr [expr 0x43C0000c] [expr 0xBBBBBBBB]
mwr [expr 0x43C00008] [expr 0x11111111]
mwr [expr 0x43C00008] [expr 0x00000000]
#Store Random Data On 3rd Target Using Virtual Base Address
mwr [expr 0x43C00000] [expr 0x33330008]
mwr [expr 0x43C0000c] [expr 0xCCCCCCCC]
mwr [expr 0x43C00008] [expr 0x11111111]
mwr [expr 0x43C00008] [expr 0x00000000]
#Store Random Data On 4th Target Using Virtual Base Address
mwr [expr 0x43C00000] [expr 0x44440004]
mwr [expr 0x43C0000c] [expr 0xDDDDDDDD]
mwr [expr 0x43C00008] [expr 0x11111111]
mwr [expr 0x43C00008] [expr 0x00000000]